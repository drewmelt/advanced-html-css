const nav = document.querySelector('.nav__list')
const burgerBtn = document.querySelector('.nav__list-wrapper .btn--burger')


document.addEventListener('click', e => {
    const burger = e.target.closest('.btn--burger')
    burgerBtn.classList.toggle('burger--active')
    if (burger) {
        nav.classList.toggle('nav-open')
    } else {
        nav.classList.remove('nav-open')
    }
})